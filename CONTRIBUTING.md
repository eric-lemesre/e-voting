# Code of Conduct

## **E-voting community programme (ECP)**

By accessing the programme material, you agree to comply with the Code of Conduct (hereinafter CoC). If you do not accept the CoC, please do not use the programme material.

## Introduction

The developers, cryptographic experts and other specialists from Swiss Post are continuously improving the e-voting system with universal verifiability. Swiss Post wants to be as transparent as possible about the functioning and security of the system and fosters collaboration with the community and the continuous improvement of the system.

For this purpose, Swiss Post has implemented the **e-voting community programme (ECP)**. It governs public access and usage of the source code and other materials of Swiss Post's e voting system. Swiss Post provides a single point of contact (SPOC) for all matters related to the ECP.

Swiss Post retains all rights to the source code and other materials, including copyright, patent rights and/or usage rights.

The CoC will be updated to reflect changes in the e-voting regulations and will be continuously improved as the community's experience grows, as well as to cover new ECP features. More information about coming changes and timeline can be found in our [FAQ](https://evoting-community.post.ch/en/help-and-contact/faq).

## What you can expect from us

- The integrity of the democratic process is at the heart of our mission.
- We prioritize security and voting secrecy.
- We will work with you to understand and validate your report.
- We recognize your contribution to improving our security.
- We respect the academic freedom of researchers.

## What we expect

- You respect the privacy of others, and do not destroy data or disrupt systems.
- Participants observe the Coordinated vulnerability disclosure policy.
- You report your findings using the provided [templates](https://evoting-community.post.ch/en/contributions).

## Organization

The ECP starts on 19.01.2021. It will remain accessible for as long as the Swiss Post e-voting system is operable.

No registration is necessary for participating in the ECP. Your participation in the ECP is anonymous. It is your choice to provide us with contact information.

The SPOC serves as a single point of contact for all participants. The [SPOC](https://evoting-community.post.ch/en/help-and-contact/support#contact) is mainly responsible for any issues related to our voting solution you would like to discuss.

## In-scope

In the scope of the ECP are the code and other materials that are available in the repositories under <https://gitlab.com/swisspost-evoting>

In case of doubt, please check with the [SPOC](https://evoting-community.post.ch/en/help-and-contact/support#contact) beforehand.

## Out of Scope

What is not defined as in-scope, is out-of-scope.

## Reporting

Reports on any findings should be filed using the provided [templates](https://evoting-community.post.ch/en/contributions).

A report should include:

- a basic description of the issue in question
- a list of affected components (e.g. xxxx) and their precise location in the ECP repository
- a step-by-step reproduction guide of the findings
- if possible: accompanying evidence, e.g. screenshots, videos, proof of concept code, dumps, etc.
- contact information (recommended)

Please note, if the [severity](REPORTING.md#defining-severity) of the finding is high or critical, the "confidential"-checkbox in the GitLab-template must be checked.

## Coordinated vulnerability disclosure policy

Findings are published on [GitLab](https://gitlab.com/swisspost-evoting) for purposes of transparency.

The reporter of the findings will be credited if the reporter agrees to its publication. The reporter may remain anonymous or use an alias.

You are free to publish your findings, but please give us a maximum of 90 days to analyse your report. As soon as we have analysed your report, we will give you a thumb up for publication. This allows us and the cantons to address the finding and to minimise risks.

## Permitted use

Under the ECP, you are entitled to examine, modify, compile and execute the source code for non-commercial or non-productive purposes, to publish papers on your findings and to use the information gained for the purpose of research, advancing knowledge, teaching, learning, or customizing the technology or modifications for personal use.

You are allowed to make a copy. The license terms, copyright notices and attributions included in the source codes or the other materials must be maintained at all times.

If you publish modifications, you must respect the coordinated vulnerability disclosure policy. Modifications must be highlighted.

The ECP does not allow you to use any information gained for commercial purposes and productive use, in particular to use any part of code in any other software and services. In case of interest, please contact the [SPOC](https://evoting-community.post.ch/en/help-and-contact/support#contact).

## Legal safe harbour: consequences of complying with the Code of Conduct

As soon as an internet test is implemented as a new feature in the ECP, Swiss Post interprets activities by participants in such internet tests that comply with the CoC as authorized access under the Swiss Penal Code and other anti-hacking and anti-circumvention laws. This includes Swiss Penal Code Articles 143, 143bis and 144bis.
Swiss Post will only file a complaint for violation of the CoC if the code or the other materials or parts thereof are used commercially or productively.
If legal action is initiated by a third party against a participant and the participant has complied with the CoC as outlined in this document, Swiss Post will take the necessary measures to make it known to the authorities that such participant's actions have been conducted in compliance with this policy.  

Any non-compliance with the CoC may result in exclusion from the ECP.

## Applicable law and jurisdiction

The Code of Conduct is governed by and construed in accordance with the laws of Switzerland.

Any dispute arising out of or relating to the Code of Conduct shall be submitted and finally resolved by the courts of Berne, Switzerland.

We are looking forward to your reports.

The e-voting team
